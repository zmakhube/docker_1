#!/bin/sh
# TeamSpeak installation in debian
# 42/WeThinkCode_ Docker_1 Project

cd $HOME
wget http://dl.4players.de/ts/releases/3.0.13.8/teamspeak3-server_linux_amd64-3.0.13.8.tar.bz2
tar -xvf teamspeak3-server_linux_amd64-3.0.13.8.tar.bz2
rm -f teamspeak3-server_linux_amd64-3.0.13.8.tar.bz2
mv teamspeak3-server_linux-amd64 teamspeak
cd teamspeak
chmod +x ts3server_startscript.sh ts3server_linux_amd64
clear
$HOME/teamspeak/./ts3server_startscript.sh start
echo "Retrieving server status..."
$HOME/teamspeak/./ts3server_startscript.sh status
exit 0
done
